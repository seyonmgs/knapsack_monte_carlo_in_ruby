require 'pp'

class Knapsack
	
	attr_reader :trialResults

	# get the  files (or items) as a array of hashes with weights and values
    #    [{value: 10, weight: 5}]
	# get the maxWeight that is tolerated by the knapsack
	# get the number of trials to be executed 

	def initialize (files, maxWeight, trials)
		@files = files
		@maxWeight = maxWeight
		@trials = trials
		@trialResults = {value: 0, weight: 0}
		@chosenFiles = Array.new(@files.size, 0) 
	end

	#This switches the binary value at an index chosen at random
	def switch_a_file (files) 
		i=rand(@files.size)
		files[i] == 0 ? files[i] = 1 : files[i] = 0
		files
	end


	#This checks if chosenFiles array. If the switch at an index is on (==1), then
	#that file is accepted. If not, it is rejected. Once accepted, the value and weight value
	#is accumulated and returned.

	def check_files (files)
		weightOfBag=0
		valueOfBag=0

		(1..files.size).each do |i|
			if (files[i-1] == 1) then
				#puts " i is #{i-1} and value is #{@files[i-1][:weight]}"
				weightOfBag = weightOfBag + @files[i-1][:weight]
				valueOfBag = valueOfBag + @files[i-1][:value]						
			end
		end
		#puts " weight in bag #{weightOfBag} and files are #{files}"

		{weight: weightOfBag, value: valueOfBag}

	end

	#This is the core heart of the monte carlo method. The trail is performed for the requested number of 
	#iterations. At the start of each iteration, a switch_a_file is called to randomnly change file switches.
	#Then based on switches, files and their values+weights are aggregated. If the aggregated weight is lesser 
	#than the max weight requested and the value is higher than values computed in prior iteratios of the trials, 
	#then that value is stored, along with the weight and the selection of files. 
	def run_trials
		(1..@trials).each do |trial|
				#pp @chosenFiles
				newFiles = switch_a_file (@chosenFiles)
				newResults = check_files (newFiles)
				if (newResults[:weight] <= @maxWeight) then
					@chosenFiles = newFiles


					if (newResults[:value] > @trialResults[:value])
										 then
							#puts "#{newResults[:value]} and curr val #{@trialResults[:value]} and files #{newFiles}"			 
							@trialResults[:value] = newResults[:value]
							@trialResults[:weight] = newResults[:weight]
							@trialResults[:chosenFiles] = newFiles.dup
					end		

				end
		end				
	end	
end


